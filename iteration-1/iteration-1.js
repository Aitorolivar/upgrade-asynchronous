// 1.1 Utiliza esta url de la api Agify 'https://api.agify.io?name=michael' para 
// hacer un .fetch() y recibir los datos que devuelve. Imprimelo mediante un 
// console.log(). Para ello, es necesario que crees un .html y un .js.


const infoFetch = async () => {
    try {
        const result = await fetch('https://api.agify.io?name=michael');
        const resultToJson = await result.json();
        return resultToJson;
    } catch (err) {
        console.log(err);
    }
}

const init = async () => {
    const resolveResponse = await infoFetch();
    console.log(resolveResponse)
}


window.onload = init(); 



// 2.1 Dado el siguiente javascript y html. Añade la funcionalidad necesaria usando 
// fetch() para hacer una consulta a la api cuando se haga click en el botón, 
// pasando como parametro de la api, el valor del input.
// const baseUrl = 'https://api.nationalize.io?name={aqui va el nombre}';

const baseUrl = 'https://api.nationalize.io?name='
const $$button = document.querySelector('button');

const getresult = async (name) => {
    try {
        const result = await fetch(`${baseUrl}${name}`)
        const resutlToJson = await result.json();
        return resutlToJson;
    } catch (err) {
        console.log(err);
    }
}

const search = async () => {
    const $$input = document.querySelector('input');
    let result = await getresult($$input.value);
    console.log(result);
    showSearch(result);
}

$$button.addEventListener('click', search);

// 2.3 En base al ejercicio anterior. Crea dinamicamente un elemento  por cada petición 
// a la api que diga...'El nombre X tiene un Y porciento de ser de Z' etc etc.
// EJ: El nombre Pepe tiene un 22 porciento de ser de ET y un 6 porciento de ser 
// de MZ.



const showSearch = (info) => {
    const newH2 = document.createElement('h2');
    newH2.innerHTML = `El nombre ${info.name} tiene un ${info.country[0].probability} porciento de ser de ${info.country[0].country_id} y un ${info.country[2].probability} porciento de ser de ${info.country[2].country_id}`;

    let newBtn = document.createElement('button');
    newBtn.textContent = 'X';

    const deletItem = () => newH2.remove();
    newBtn.addEventListener('click', deletItem);

    document.body.appendChild(newH2);
    newH2.appendChild(newBtn);
};
